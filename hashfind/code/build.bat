@echo off

REM SET CompilerFlags=/nologo /Z7 /Od /Oi /FC /fp:fast /favor:INTEL64 /MTd /Wall /W4 /wd4710 /wd4189 /wd4100 /DDEBUG=1 /D_CRT_SECURE_NO_WARNINGS=1
SET LinkerFlags=/incremental:no /opt:ref

SET CompilerFlags=/nologo /O2 /Oi /FC /fp:fast /favor:INTEL64 /MT /D_CRT_SECURE_NO_WARNINGS=1

IF NOT EXIST build mkdir build
pushd build

cl.exe %CompilerFlags% ..\hashfind\code\main.c /link %LinkerFlags%

popd
