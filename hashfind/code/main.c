/**
 * HashFind
 *
 * Use a hash table to search for keys within a haystack.
 * 
 * Usage:
 * No command line arguments puts it into interactive mode.
 * Two file names will search for items from the second file in the first file.
 **/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#ifdef DEBUG
#define Assert(Expression) if(!(Expression)) { *(int *)0 = 0; }
#else
#define Assert(Expression)
#endif
#define ArrayCount(Array) (sizeof((Array)) / sizeof((Array)[0]))

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;
typedef int32_t b32;
typedef float r32;
typedef double r64;

enum
{
    DEFAULT_TABLE_SIZE = 13337,
    READ_BUFFER_SIZE = 1024,
};

typedef struct hash_table* table_t;
struct hash_table
{
    u32 Size;
    u32 Count;
    struct binding
    {
        void *Key;
        struct binding *Next;
    } **Bindings;

    u32 (*HashFunction)(void *, u32);
    u32 (*CompareFunction)(void *, void *);
};

inline table_t
TableNew(u32 TableSize,
         u32 (*HashFunction)(void *, u32),
         u32 (*CompareFunction)(void *, void *))
{
    if(!TableSize)
    {
        TableSize = DEFAULT_TABLE_SIZE;
    }

    table_t Result = (table_t)malloc(sizeof(*Result) + (sizeof(*Result->Bindings)*TableSize));
    Assert(Result);
    Result->Size = TableSize;
    Result->Count = 0;
    Result->Bindings = (struct binding **)(Result + 1);
    Result->HashFunction = HashFunction;
    Result->CompareFunction = CompareFunction;

    for(u32 Binding = 0;
        Binding < Result->Size;
        ++Binding)
    {
        *(Result->Bindings + Binding) = 0;
    }

    return(Result);
}

inline void *
TableFind(table_t Table, void *Key)
{
    void *Result = 0;

    u32 TableIndex = Table->HashFunction(Key, Table->Size);
    Assert(TableIndex < Table->Size);
    for(struct binding *Binding = *(Table->Bindings + TableIndex);
        Binding != 0;
        Binding = Binding->Next)
    {
        if(Table->CompareFunction(Key, Binding->Key) == 0)
        {
            Result = Binding->Key;
            break;
        }
    }

    return(Result);
}

inline void *
TableInsert(table_t Table, void *Key)
{
    void *KeyFound = TableFind(Table, Key);
    if(KeyFound == 0)
    {
        u32 TableIndex = Table->HashFunction(Key, Table->Size);

        struct binding *NewBinding = (struct binding *)malloc(sizeof(*NewBinding));
        NewBinding->Key = Key;
        NewBinding->Next = *(Table->Bindings + TableIndex);
        *(Table->Bindings + TableIndex) = NewBinding;

        KeyFound = Key;
    }

    return(KeyFound);
}

static inline u32
DefaultHash(void *KeyIn, u32 TableSize)
{
    u32 Hash = 0;
    u32 A = 31415;
    u32 B = 27183;

    u8 *Key = (u8 *)KeyIn;
    for(; *Key != 0; ++Key)
    {
        Hash = (A*Hash + *Key) % TableSize;
        A = A*B%(TableSize - 1);
    }

    return(Hash);
}

static inline u32
DefaultCompare(void *A, void *B)
{
    u8 *Left = (u8 *)A;
    u8 *Right = (u8 *)B;

    while(*Left == *Right)
    {
        if(*Left == 0)
        {
            return 0;
        }
        ++Left, ++Right;
    }

    return(*Left - *Right);
}

int
main(i32 ArgCount, char **Args)
{
    if((ArgCount > 1) && (ArgCount != 3))
    {
        fprintf(stderr, "Usage: %s haystack needles\n", Args[0]);
        return(1);
    }


    FILE *SourceFile = stdin;
    FILE *TargetFile = stdin;
    b32 IsInteractive = (ArgCount == 1);
    if(!IsInteractive)
    {
        SourceFile = fopen(Args[1], "r");
        if(!SourceFile)
        {
            fprintf(stderr, "Failed to open file '%s'.\n", Args[1]);
            return(2);
        }

        TargetFile = fopen(Args[2], "r");
        if(!TargetFile)
        {
            fprintf(stderr, "Failed to open file '%s'.\n", Args[2]);
            return(2);
        }
    }

    table_t Table = TableNew(0, DefaultHash, DefaultCompare);
    char ReadBuffer[READ_BUFFER_SIZE] = {0};

    if(IsInteractive)
    {
        fprintf(stderr, "Enter source strings:\n");
    }
    while(fgets(ReadBuffer, READ_BUFFER_SIZE, SourceFile) != 0)
    {
        u32 StringLength = (u32)strlen(ReadBuffer);
        u8 *NewString = (u8 *)malloc(StringLength);
        memcpy(NewString, ReadBuffer, StringLength);
        NewString[StringLength - 1] = 0;

        TableInsert(Table, NewString);
    }

    if(IsInteractive)
    {
        fprintf(stderr, "Enter strings to find:\n");
    }
    while(fgets(ReadBuffer, READ_BUFFER_SIZE, TargetFile) != 0)
    {
        u32 StringLength = (u32)strlen(ReadBuffer);
        ReadBuffer[StringLength - 1] = 0;
        u8 *Found = TableFind(Table, ReadBuffer);
        if(Found == 0)
        {
            printf("%s\n", ReadBuffer);
        }
    }

#if 0
    for(u32 Index = 0;
        Index < Table->Size;
        ++Index)
    {
        u32 ChainIndex = 0;
        for(struct binding *Binding = *(Table->Bindings + Index);
            Binding != 0;
            Binding = Binding->Next)
        {
            printf("{%d, %d}: %s\n", Index, ChainIndex, (u8 *)Binding->Key);
        }
    }
#endif

    return(0);
}
